import React , { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function AppointmentList(props) {
    const { status } = props;
    const [appointments, setAppointments] = useState([]);

    const fetchAppointmentsData = async () => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${status}`;
        const response = await fetch(appointmentUrl);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    const finish = async (event) => {
        event.preventDefault();
        const appointmentUrl = `http://localhost:8080/api/appointment/${event.target.value}/finish`
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const data = await response.json();
            fetchAppointmentsData();
        }
    };

    const cancel = async (event) => {
        event.preventDefault();
        const appointmentUrl = `http://localhost:8080/api/appointment/${event.target.value}/cancel`
        const fetchConfig = {
            method: "put",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const data = await response.json();
            fetchAppointmentsData();
        }
    };

    const pastOrCurrent = (appointment) => {
        if (status === "") {
            return (<>
                <button className="btn-success" onClick={finish} value={appointment.id}>Finish</button>
                <div></div>
                <button className="btn-danger" onClick={cancel} value={appointment.id}>Cancel</button>
            </>);
        }
        else { return (<div>{appointment.status}</div>); }
    }

    useEffect(() => {fetchAppointmentsData();}, [status]);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="text-center">VIN</th>
                        <th className="text-center">Customer</th>
                        <th className="text-center">Date</th>
                        <th className="text-center">Time</th>
                        <th className="text-center">Technician</th>
                        <th className="text-center">Reason</th>
                        <th className="text-center">Status</th>
                        <th className="text-center">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments?.map(appointment => {return (
                        <tr key={appointment.id}>
                            <td className="text-center">{appointment.vin}</td>
                            <td className="text-center">{appointment.customer}</td>
                            <td className="text-center">{appointment.date}</td>
                            <td className="text-center">{appointment.time}</td>
                            <td className="text-center">{appointment.reason}</td>
                            <td className="text-center">{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td className="text-center">{pastOrCurrent(appointment)}</td>
                            <td className="text-center">
                                <Link to={`/appointments/edit/${appointment.id}/`} relative="path">Edit</Link>
                            </td>
                        </tr>
                    );})}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentList;
