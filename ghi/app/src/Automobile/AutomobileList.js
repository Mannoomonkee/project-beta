import { useEffect, useState } from 'react';

function AutomobileList() {
  const [automobile, setAutomobile] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/');

    if (response.ok) {
      const data = await response.json();
      setAutomobile(data.autos)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  //handeling delete button
  const handleDelete = async (event) => {
    const url = `http://localhost:8100/api/automobiles/${event.target.id}`
    const getConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json"
      }
    }
    const response = await fetch(url, getConfigs)
    if (response.ok) {
      getData();
    } else {
      alert("Automobile was not deleted");
    }
  };

  const renderSoldStatus = (sold) => {
    return sold ? "Yes" : "No";
  };

  return (
      <table className="table table-striped">
      <thead>
      <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Manufacturer</th>
        </tr>
      </thead>
      <tbody>
        {automobile?.map(a => {
          return (
            <tr key={a.id}>
              <td>{a.vin}</td>
              <td>{a.color}</td>
              <td>{a.year}</td>
              <td>{a.model.name}</td>
              <td>{a.model.manufacturer.name}</td>
              <td><button onClick={handleDelete} id={a.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AutomobileList;

