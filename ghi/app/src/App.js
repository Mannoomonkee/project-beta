import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';



import CustomerList from './Customer/CustomerList';
import CustomerForm from './Customer/CustomerForm';
import SalespeopleList from './Salespeople/SalespeopleList';
import SalespeopleForm from './Salespeople/SalespeopleForm';
import SaleList from './Sale/SaleList';
import SaleForm from './Sale/SaleForm';
import SalespersonHistory from './Saleshistory/Saleshistory';
import ManufacturerList from './Manufacturer/ManufacturerList';
import ManufacturerForm from './Manufacturer/ManufacturerForm';
import VehicleList from './Vehicle/VehicleList';
import VehicleForm from './Vehicle/VehicleForm';
import AutomobileList from './Automobile/AutomobileList';
import AutomobileForm from './Automobile/AutomobileForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians/">
            <Route index element={<TechnicianList />} />
            <Route path="create/" element={<TechnicianForm />} />
            <Route path="edit/:id/" element={<TechnicianForm />} />
          </Route>
          <Route path="manufacturers/">
            <Route index element={<ManufacturerList />} />
            <Route path="create/" element={<ManufacturerForm />} />
            <Route path="edit/:id/" element={<ManufacturerForm />} />
          </Route>
          <Route path="models/">
            <Route index element={<VehicleList />} />
            <Route path="create/" element={<VehicleForm />} />
            <Route path="edit/:id/" element={<VehicleForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobileList />} />
            <Route path="create/" element={<AutomobileForm />} />
            <Route path="edit/:vin/" element={<AutomobileForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentList />} />
            <Route path="create/" element={<AppointmentForm />} />
            <Route path="edit/:id/" element={<AppointmentForm />} />
            </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleList />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="new" element={<SalespeopleForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SaleList />} />
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="saleshistory">
            <Route index element={<SalespersonHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
