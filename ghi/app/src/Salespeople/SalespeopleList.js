import { useEffect, useState } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salesperson)
      console.log(data)
    }
  }
  useEffect(()=>{
    getData()
  }, [])

  //handeling delete button
  const handleDelete = async (event) => {
    const url = `http://localhost:8090/api/salespeople/${event.target.id}`
    const getConfigs = {
        method: "Delete",
        headers: {
            "Content-Type": "application/json"
        }
    }
    const response = await fetch(url, getConfigs)
    if (response.ok) {
        getData();
    } else {
        alert("Salesperson was not deleted");
    }
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>
        {salespeople?.map(salesperson => {
          return(
            <tr key={salesperson.id}>
                <td>{ salesperson.first_name }</td>
                <td>{ salesperson.last_name }</td>
                <td>{ salesperson.employee_id }</td>
                <td><button onClick={handleDelete} id={salesperson.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SalespeopleList;

