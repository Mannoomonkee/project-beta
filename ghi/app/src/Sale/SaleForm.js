import React, {useState, useEffect } from 'react';

function SaleForm() {
  const [sales, setSales] = useState([])
  const [automobiles, setAutomobiles] = useState([])
  const [salespeople, setSalespeople] = useState([])
  const [customers, setCustomers] = useState([])
  const [formData, setFormData] = useState({
    automobile_vin: '',
    salesperson_id: '',
    customer_id: '',
    price: '',
  })

  const getDataSale = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSales(data.sale);
    }
  }

  const getDataAutomobile = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }

  const getDataSalespeople = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salesperson);
    }
  }

  const getDataCustomer = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customer);
    }
  }

  useEffect(() => {
    getDataSale();
    getDataAutomobile();
    getDataSalespeople();
    getDataCustomer();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
    },
  };

  const response = await fetch(locationUrl, fetchConfig);

  if (response.ok) {
    setFormData({
      automobile_vin: '',
      salesperson_id: '',
      customer_id: '',
      price: '',
    });
  }
}

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
          <div className="mb-3">
              <select onChange={handleFormChange} value={formData.automobile_vin} required name="automobile_vin" id="automobile_vin" className="form-select">
                <option value="">VIN</option>
                {automobiles?.map(automobile => {
                  return (
                    <option key={automobile.vin} value={automobile.vin}>
                        {automobile.vin}
                    </option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.salesperson_id} required name="salesperson_id" id="salesperson_id" className="form-select">
                <option value="">Salesperson</option>
                {salespeople?.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.customer_id} required name="customer_id" id="customer_id" className="form-select">
                <option value="">Customer</option>
                {customers?.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>
                        {customer.first_name} {customer.last_name}
                    </option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;

