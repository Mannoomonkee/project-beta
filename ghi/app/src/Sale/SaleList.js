import { useEffect, useState } from 'react';

function SaleList() {
  const [sale, setSale] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');

    if (response.ok) {
      const data = await response.json();
      setSale(data.sale)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  //handeling delete button
  const handleDelete = async (event) => {
    const url = `http://localhost:8090/api/sales/${event.target.id}`
    const getConfigs = {
        method: "Delete",
        headers: {
            "Content-Type": "application/json"
        }
    }
    const response = await fetch(url, getConfigs)
    if (response.ok) {
        getData();
    } else {
        alert("Sale was not deleted");
    }
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson Employee ID</th>
          <th>Salesperson Name</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {sale?.map(sale => {
          return (
            <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
                <td><button onClick={handleDelete} id={sale.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SaleList;
