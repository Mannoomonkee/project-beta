import React, {useState, useEffect } from 'react';

function CustomerForm() {
  const [customers, setCustomers] = useState([])
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  })

  const getData = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customer);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8090/api/customers/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
    },
  };

  const response = await fetch(locationUrl, fetchConfig);

  if (response.ok) {
    setFormData({
      first_name: '',
      last_name: '',
      address: '',
      phone_number: '',
    });
  }
}

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.first_name} placeholder="First_name" required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.last_name} placeholder="Last_name" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="mb-3">
              <label htmlFor="address">Address</label>
              <textarea onChange={handleFormChange} value={formData.address} className="form-control" id="address" rows="3" name="address" className="form-control"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.phone_number} placeholder="Phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
              <label htmlFor="phone_number">Phone_number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;

