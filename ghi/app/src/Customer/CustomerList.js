import { useEffect, useState } from 'react';

function CustomerList() {
  const [customer, setCustomer] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/customers/');

    if (response.ok) {
      const data = await response.json();
      setCustomer(data.customer)
      console.log(customer)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  //handeling delete button
  const handleDelete = async (event) => {
    const url = `http://localhost:8090/api/customers/${event.target.id}`
    const getConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json"
      }
    }
    const response = await fetch(url, getConfigs)
    if (response.ok) {
      getData();
    } else {
      alert("Customer was not deleted");
    }
  };

  return (
      <table className="table table-striped">
      <thead>
      <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Phone Number</th>
          <th>Address</th>
        </tr>
      </thead>
      <tbody>
        {customer?.map(c => {
          return (
            <tr key={c.id}>
              <td>{c.first_name}</td>
              <td>{c.last_name}</td>
              <td>{c.phone_number}</td>
              <td>{c.address}</td>
              <td><button onClick={handleDelete} id={c.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default CustomerList;

