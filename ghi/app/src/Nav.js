import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="techdropdown" data-bs-toggle="dropdown" aria-expanded="false">Technicians</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="techdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/">Technicians List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/technicians/create/">Create Technician</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="manufacturerdropdown" data-bs-toggle="dropdown" aria-expanded="false">Manufacturers</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="manufacturerdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/">Manufacturers List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/manufacturers/create/">Create Manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="autodropdown" data-bs-toggle="dropdown" aria-expanded="false">Automobiles</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="autodropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/">Automobiles List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/automobiles/create/">Create Automobile</NavLink>
                </li>
              </ul>
            </li>
            <li className="dropdown nav-item">
              <div className="btn dropdown-toggle nav-link" type="button" id="Appointmentdropdown" data-bs-toggle="dropdown" aria-expanded="false">Appointments</div>
              <ul className="dropdown-menu bg-success" aria-labelledby="appointmentdropdown">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/">Appointment List</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/appointments/create/">Create Appoointment</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">Create a manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new">Create a Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/new">Create an Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/new">Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">Add a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new">Add a Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/saleshistory">Sales History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
