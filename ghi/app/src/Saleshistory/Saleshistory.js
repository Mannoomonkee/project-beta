import { useEffect, useState } from 'react';

function SalespersonHistory() {
    const [sale, setSale] = useState([]);
    const [filterValue, setFilterValue] = useState("");

    const getData = async () => {
      const response = await fetch('http://localhost:8090/api/sales/');


    if (response.ok) {
        const data = await response.json();
        const unsoldAutomobiles = data.sale.filter((sale) => sale.status !== "sold");
        setSale(unsoldAutomobiles)
      }
    }

    useEffect(() => {
      getData()
    }, [])

    const filteredPeople = () => {
        return sale.filter((sale) =>
            sale.salesperson.first_name.toLowerCase().includes(filterValue.toLowerCase()) ||
            sale.salesperson.last_name.toLowerCase().includes(filterValue.toLowerCase()) ||
            sale.customer.first_name.toLowerCase().includes(filterValue.toLowerCase()) ||
            sale.customer.last_name.toLowerCase().includes(filterValue.toLowerCase()) ||
            sale.automobile.vin.toLowerCase().includes(filterValue.toLowerCase()) ||
            sale.price.includes(filterValue)
        );
    };


    const handleFilterValueChange = (e) => {
        const { value } = e.target;
        setFilterValue(value);
    };

    return (
        <div className="row">
            <h1 className="text-left">Salesperson History</h1>
            <input
                onChange={handleFilterValueChange}
                value={filterValue}
                placeholders="Search"
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredPeople().map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonHistory;

