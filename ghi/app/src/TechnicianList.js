import React , { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const fetchTechData = async () => {
        const techUrl = "http://localhost:8080/api/technicians/";
        const response = await fetch(techUrl);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {fetchTechData();}, []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="text-center">First Name</th>
                        <th className="text-center">Last Name</th>
                        <th className="text-center">Employee ID</th>
                        <th className="text-center">Edit</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(tech => {return (
                        <tr key={tech.employee_id}>
                            <td className="text-center">{tech.first_name}</td>
                            <td className="text-center">{tech.last_name}</td>
                            <td className="text-center">{tech.employee_id}</td>
                            <td className="text-center">
                                <Link to={`/technicians/edit/${tech.id}/`} relative="path">Edit</Link>
                            </td>
                        </tr>
                    );})}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
