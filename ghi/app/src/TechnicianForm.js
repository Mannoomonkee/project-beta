import React , { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function DelButton ({ id }) {
    const navigate = useNavigate();
    const deleteInstance = async () => {
        const delUrl = `http://localhost:8080/api/technicians/${id}/`;
        const editConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(delUrl, editConfig);
        if (response.ok) {
            const delTech = await response.json();
            navigate("/technicians/", { replace: true });
        }
    }

    if (id !== undefined) {
        return (<div><button className="btn btn-primary mb-3" type="button" onClick={deleteInstance}>Delete</button></div>);
    }
    return (<></>);
}

function TechnicianForm() {
    const navigate = useNavigate();
    const [{ id }, setId] = useState(useParams());
    const [techData, setTechData] = useState({
        first_name:"",
        last_name:"",
        employee_id:"",
    });

    const firstRender = async () => {
        if (id !== undefined) {
            const detailUrl = `http://localhost:8080/api/technicians/${id}/`;
            const response = await fetch(detailUrl);
            if (response.ok) {
                const editTech = await response.json();
                setTechData({
                    first_name: editTech.technician.first_name,
                    last_name: editTech.technician.last_name,
                    employee_id: editTech.technician.employee_id,
                });
            }
        }
    };

    const handleInput = (event) => {
        setTechData({
            ...techData,
            [event.target.name]: event.target.value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (id !== undefined) {
            const detailUrl = `http://localhost:8080/api/technicians/${id}/`;
            const editConfig = {
                method: "put",
                body: JSON.stringify(techData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(detailUrl, editConfig);
            if (response.ok) {
                const editTech = await response.json();
                navigate("/technicians/", { replace: true });
            }
        } else {
            const technicianUrl = "http://localhost:8080/api/technicians/";
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(techData),
                headers: {
                    "Content-Type": "application/json",
                }
            };
            const response = await fetch(technicianUrl, fetchConfig);
            if (response.ok) {
                const newTechnician = await response.json();
                setTechData({
                    first_name:"",
                    last_name:"",
                    employee_id:"",
                });
            }
        }
    }

    useEffect(() => {firstRender();}, [id]);

    return (
        <div className="container">
            <div className="row">
                <div className="shadow">
                    <h1>Technician Information</h1>
                    <form onSubmit={handleSubmit} id="tech-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={techData.first_name} placeholder="" name="first_name" id="first_name" type="text" className="form-control" required />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={techData.last_name} placeholder="" name="last_name" id="last_name" type="text" className="form-control" required />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleInput} value={techData.employee_id} placeholder="" name="employee_id" id="employee_id" type="number" className="form-control" required />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary mb-3" type="submit">Submit</button>
                        <DelButton id={id} />
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
