import { useEffect, useState } from 'react';

function ManufacturerList() {
  const [manufacturer, setManufacturer] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');

    if (response.ok) {
      const data = await response.json();
      setManufacturer(data.manufacturers)
      console.log(manufacturer)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  //handeling delete button
  const handleDelete = async (event) => {
    const url = `http://localhost:8100/api/manufacturers/${event.target.id}`
    const getConfigs = {
      method: "Delete",
      headers: {
        "Content-Type": "application/json"
      }
    }
    const response = await fetch(url, getConfigs)
    if (response.ok) {
      getData();
    } else {
      alert("Manufacturer was not deleted");
    }
  };

  return (
      <table className="table table-striped">
      <thead>
      <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        {manufacturer?.map(m => {
          return (
            <tr key={m.id}>
              <td>{m.name}</td>
              <td><button onClick={handleDelete} id={m.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ManufacturerList;
