import { useEffect, useState } from 'react';

function VehicleList() {
  const [vehicle, setVehicle] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');

    if (response.ok) {
      const data = await response.json();
      setVehicle(data.models)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  //handeling delete button
  const handleDelete = async (event) => {
    const url = `http://localhost:8100/api/models/${event.target.id}`
    const getConfigs = {
        method: "Delete",
        headers: {
            "Content-Type": "application/json"
        }
    }
    const response = await fetch(url, getConfigs)
    if (response.ok) {
        getData();
    } else {
        alert("Vehicle was not deleted");
    }
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {vehicle?.map(v => {
          return (
            <tr key={v.id}>
                <td>{v.name}</td>
                <td>{v.manufacturer.name}</td>
                <td><img src={v.picture_url} width="150" /></td>
                <td><button onClick={handleDelete} id={v.id} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default VehicleList;

