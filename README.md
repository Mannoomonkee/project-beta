# CarCar

Team:

* Xiaodi - Sales
* Mariam - Service

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

I developed three models along with an "AutomobileVO" model. Subsequently, I established the poller code to retrieve automobile content from the Inventory. Furthermore, I registered my models within the administrative framework. In order to facilitate RESTful APIs for displaying salesperson, customer, and sales listings, as well as enabling operations such as deletion, creation, and updating, I implemented a set of view functions.

On the frontend side, I created React components to present the aforementioned lists and their corresponding details. Additionally, I designed a form to facilitate the creation and deletion of entries. Finally, I established appropriate routing for all links and navigation bars, ensuring smooth navigation between the components and thereby completing the website and the overall project.
