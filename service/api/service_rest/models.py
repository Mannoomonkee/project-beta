from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=150)
    status = models.CharField(max_length=150, default="created")
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=150)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def finish(self):
        self.status = "finished"
        self.save()

    def cancel(self):
        self.status = "canceled"
        self.save()
